package com.titan;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import net.sf.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.titan.mainframe.MainFrame;
import com.titan.mainframe.ServerTreeNode;
import com.titanserver.structure.TitanServerDefinition;

public class TitanCommonLib {

	public static String getJSONString(JSONObject obj, String key, String returnValue) {
		try {
			return obj.getString(key);
		} catch (Exception ex) {
			return returnValue;
		}
	}

	public static String getCurrentServerIP() {
		if (MainFrame.serverTree.getSelectionCount() == 0 || !(MainFrame.serverTree.getLastSelectedPathComponent() instanceof ServerTreeNode)) {
			return Global.primaryServerIP;
		} else {
			TitanServerDefinition server = ((ServerTreeNode) MainFrame.serverTree.getLastSelectedPathComponent()).server;
			return server.ip;
		}
	}

	public static String getXPath(String xml, String xpathStr) {
		String str = null;
		ByteArrayInputStream is;
		if (xml.toLowerCase().startsWith("<?xml version=\"1.0\" encoding=\"utf-8\"?>")) {
			is = new ByteArrayInputStream(xml.getBytes());
		} else {
			is = new ByteArrayInputStream(("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + xml).getBytes());
		}
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true); // never forget this!
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(is);
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile(xpathStr);
			Object result = expr.evaluate(doc, XPathConstants.NODESET);
			NodeList nodes = (NodeList) result;
			if (nodes.getLength() > 0) {
				str = nodes.item(0).getNodeValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

	public static NodeList getXPathNodeList(String xml, String xpathStr) {
		ByteArrayInputStream is;
		if (xml.toLowerCase().startsWith("<?xml version=\"1.0\" encoding=\"utf-8\"?>")) {
			is = new ByteArrayInputStream(xml.getBytes());
		} else {
			is = new ByteArrayInputStream(("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + xml).getBytes());
		}
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true); // never forget this!
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(is);
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile(xpathStr);
			Object result = expr.evaluate(doc, XPathConstants.NODESET);
			return (NodeList) result;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static double round(double d, int noOfDecimal) {
		return Math.round(d * Math.pow(10, noOfDecimal)) / Math.pow(10, noOfDecimal);
	}

	public static String sendPost(String url, Hashtable ht) throws Exception {
		URL urlObj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		//		con.setRequestProperty("User-Agent", USER_AGENT);
		//		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		String urlParameters = "";

		Enumeration ee = ht.keys();
		String key = null;
		while (ee.hasMoreElements()) {
			key = (String) ee.nextElement();
			Object obj = ht.get(key);
			urlParameters += key + "=" + obj + "&";
		}

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		return response.toString();
	}

	public static String formatJSon(String str) {
		JsonParser parser = new JsonParser();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonElement el = parser.parse(str);
		return gson.toJson(el);
	}
}
