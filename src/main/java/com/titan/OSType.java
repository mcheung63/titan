package com.titan;

import java.awt.Image;

public class OSType implements Comparable<OSType> {
	public String name;
	public Image image;

	public OSType(String name, Image image) {
		this.name = name;
		this.image = image;
	}

	@Override
	public int compareTo(OSType type) {
		return name.compareToIgnoreCase(type.name);
	}

	public String toString() {
		return name;
	}
}
