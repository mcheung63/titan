package com.titan.mainframe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.io.IOUtils;

import com.apple.eawt.ApplicationEvent;
import com.apple.eawt.ApplicationListener;
import com.peterswing.CommonLib;
import com.peterswing.FilterTreeModel;
import com.peterswing.advancedswing.ribbonbar.JRibbonBigButton;
import com.peterswing.advancedswing.ribbonbar.JRibbonButton;
import com.peterswing.advancedswing.ribbonbar.JRibbonLabel;
import com.peterswing.advancedswing.ribbonbar.JRibbonPanel;
import com.peterswing.advancedswing.ribbonbar.JRibbonSeparator;
import com.titan.Global;
import com.titan.LicenseDialog;
import com.titan.Titan;
import com.titan.TitanCommonLib;
import com.titan.TitanSetting;
import com.titan.WelcomePanel;
import com.titan.communication.CommunicateLib;
import com.titan.flavorpanel.FlavorPanel;
import com.titan.instancepanel.LaunchInstanceDialog;
import com.titan.keystonepanel.KeystonePanel;
import com.titan.sdn.SDNPanel;
import com.titan.serverpanel.MainServerPanel;
import com.titan.settingpanel.SettingPanel;
import com.titan.storagepanel.StoragePanel;
import com.titan.thread.TitanServerUpdateThread;
import com.titan.vm.VMMainPanel;
import com.titancommon.Command;
import com.titancommon.HttpResult;
import com.titancommon.ReturnCommand;

public class MainFrame extends JFrame implements ApplicationListener {
	private JPanel contentPane;
	JPanel mainContentPanel = new JPanel();
	JSplitPane splitPane = new JSplitPane();
	JLabel mainScreenLabel = new JLabel();
	private WelcomePanel welcomePanel;
	public static JTree serverTree;
	ZoneTextTreeNode serverRoot = new ZoneTextTreeNode("Servers");
	public FilterTreeModel computeTreeModel = new FilterTreeModel(new ZoneTreeModel(serverRoot));
	ZoneTextTreeNode zoneRoot = new ZoneTextTreeNode("Zone", new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/weather_clouds.png")));
	public FilterTreeModel zoneTreeModel = new FilterTreeModel(new ZoneTreeModel(zoneRoot));
	MainServerPanel mainServerPanel;
	Color selectedBorderColor = new Color(175, 211, 253);
	JTabbedPane tabbedPane = new JTabbedPane();
	private JTree zoneTree;
	private JPanel ribbonPanel;
	private JTabbedPane ribbonTabbedPane;
	private JRibbonPanel serverPanel;
	private JRibbonPanel vmPanel;
	private JRibbonPanel keystonePanel;
	private JRibbonPanel flavorPanel;
	private JRibbonPanel storagePanel;
	private JRibbonPanel networkPanel;
	private JRibbonPanel settingPanel;
	private JRibbonPanel logoPanel;
	private JButton launchButton;
	private JButton pauseButton;
	private JButton unpauseButton;
	private JButton remoteButton;
	private JButton suspendButton;
	private JButton resumeButton;
	private JButton snapshotButton;
	private JButton deleteButton;
	private JButton logButton;
	private JButton softRebootButton;
	private JButton hardRebootButton;
	private JButton advanceButton;
	private JRibbonSeparator ribbonSeparator;
	private JRibbonSeparator ribbonSeparator_1;
	private JRibbonSeparator ribbonSeparator_2;
	private JButton macroButton;
	private JButton createMacroButton;
	private JButton performanceMeterButton;
	private JButton executionMapButton;
	private JRibbonSeparator ribbonSeparator_3;
	private JButton logoutButton;
	boolean windowOpened;
	private JButton stopButton;
	private JButton setGroupNameButton;
	private JButton addUserButton;
	private JButton editUserButton;
	private JButton deleteUserButton;
	private JButton detailUserButton;
	private JRibbonLabel lblUser;
	private JRibbonSeparator ribbonSeparator_4;
	private JButton addRoleButton;
	private JButton editRoleButton;
	private JRibbonLabel rbnlblRole;
	private JButton btnDeleteRole;
	private JRibbonSeparator ribbonSeparator_5;
	private JButton createTenantButton;
	private JButton deleteTenantButton;
	private JRibbonLabel rbnlblTenant;
	private JButton assignRoleButton;
	private JButton changePasswordButton;
	private JButton selectAllVMButton;
	private JButton unselectAllButton;
	private JButton uploadImageButton;
	private JButton btnDelete;
	private JButton btnChangeName;
	private JButton btnPublicprivate;
	private JRibbonLabel rbnlblImage;
	private JRibbonSeparator ribbonSeparator_6;
	private JButton btnCreateVolume;
	private JButton btnDelete_1;
	private JButton btnDetail;
	private JButton btnAttach;
	private JButton btnDetachToVm;
	private JButton btnDeleteVolumeType;
	private JButton btnAddVolumeType;
	private JRibbonLabel rbnlblVolume;
	private JRibbonBigButton rbnbgbtnAddServer;
	private JRibbonBigButton rbnbgbtnDeleteServer;
	private JRibbonBigButton rbnbgbtnCreateFlavor;
	private JRibbonBigButton btnDeleteFlavor;
	private JRibbonBigButton rbnbgbtnSystemSetting;
	private JRibbonBigButton rbnbgbtnDatabase;
	private JRibbonButton rbnbtnAddGroup;
	private JRibbonButton rbnbtnEditGroup;
	private JRibbonButton rbnbtnDeleteGroup;

	public MainFrame() {
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.toLowerCase().contains("mac")) {
			com.apple.eawt.Application macApp = com.apple.eawt.Application.getApplication();
			macApp.addApplicationListener(this);
		}
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				handleQuit();
			}

			@Override
			public void windowOpened(WindowEvent e) {
				windowOpened = true;
			}
		});
		setTitle("Titan " + Global.version);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		if (TitanSetting.getInstance().width == 0 || TitanSetting.getInstance().height == 0) {
			setBounds(TitanSetting.getInstance().x, TitanSetting.getInstance().y, 1200, 700);
		} else {
			setBounds(TitanSetting.getInstance().x, TitanSetting.getInstance().y, TitanSetting.getInstance().width, TitanSetting.getInstance().height);
		}

		setIconImage(new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/titan_icon.png")).getImage());

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		splitPane.setDividerLocation(TitanSetting.getInstance().mainframeDivX);
		contentPane.add(splitPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(0, 0));

		JLabel logoLabel = new JLabel();
		logoLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				mainContentPanel.removeAll();
				mainContentPanel.add(welcomePanel, BorderLayout.CENTER);
				mainContentPanel.updateUI();
			}
		});

//		try {
//			BufferedImage b = ImageIO.read(MainFrame.class.getResource("/com/titan/image/titanLogo.png"));
			//			Image i = b.getScaledInstance((int) (b.getWidth() * 0.6), (int) (b.getHeight() * 0.6), Image.SCALE_SMOOTH);
			logoLabel.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/titanLogo.png")));
//		} catch (IOException e1) {
//			e1.printStackTrace();
//		}
//		logoLabel.setMaximumSize(new Dimension(150, 150));

		JPanel controlPanel = new JPanel();
		controlPanel.setBackground(new Color(239, 249, 255));
		controlPanel.setOpaque(true);
		panel.add(controlPanel, BorderLayout.CENTER);
		controlPanel.setLayout(new BorderLayout());

		JScrollPane computeScrollPane = new JScrollPane();
		tabbedPane.addTab("Compute", computeScrollPane);
		controlPanel.add(tabbedPane, BorderLayout.CENTER);

		serverTree = new JTree();
		serverTree.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//				Object obj = mainContentPanel.getComponent(0);
				//				if (obj instanceof VMMainPanel) {
				//					((MainPanel) obj).refresh();
				//				}
			}
		});
		serverTree.setModel(computeTreeModel);
		serverTree.setCellRenderer(new ZoneTreeRenderer());
		serverTree.setRootVisible(false);
		computeScrollPane.setViewportView(serverTree);
		updateComputeTree();

		JScrollPane zoneScrollPane = new JScrollPane();
		tabbedPane.addTab("Zone", zoneScrollPane);

		zoneTree = new JTree();
		zoneTree.setModel(zoneTreeModel);
		zoneTree.setCellRenderer(new ZoneTreeRenderer());
		zoneScrollPane.setViewportView(zoneTree);
		updateZoneTree();
		splitPane.setLeftComponent(panel);

		splitPane.setRightComponent(mainContentPanel);
		mainContentPanel.setLayout(new BorderLayout(0, 0));

		welcomePanel = new WelcomePanel();
		mainContentPanel.add(welcomePanel, BorderLayout.CENTER);
		welcomePanel.setLayout(new BorderLayout(0, 0));
		welcomePanel.add(mainScreenLabel, BorderLayout.CENTER);
		mainScreenLabel.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/mainscreen.png")));

		JPanel panel_1 = new JPanel();
		welcomePanel.add(panel_1, BorderLayout.SOUTH);

		JButton licenseButton = new JButton("License");
		licenseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InputStream in = MainFrame.class.getResourceAsStream("/com/titan/license.txt");
				try {
					LicenseDialog dialog = new LicenseDialog(MainFrame.this, IOUtils.toString(in));
					CommonLib.centerDialog(dialog);
					dialog.setVisible(true);
				} catch (IOException e1) {
					e1.printStackTrace();
				} finally {
					IOUtils.closeQuietly(in);
				}
			}
		});
		panel_1.add(licenseButton);

		ribbonPanel = new JPanel();
		contentPane.add(ribbonPanel, BorderLayout.NORTH);
		ribbonPanel.setLayout(new BorderLayout(0, 0));

		ribbonTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		ribbonTabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				//				if (!windowOpened) {
				//					return;
				//				}
				String tab = ribbonTabbedPane.getTitleAt(ribbonTabbedPane.getSelectedIndex());
				if (tab.equals("Server")) {
					if (mainServerPanel == null || !mainServerPanel.serverPanel.jprogressBarDialog.isActive()) {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								mainContentPanel.removeAll();
								mainServerPanel = new MainServerPanel(MainFrame.this);
								mainContentPanel.add(mainServerPanel, BorderLayout.CENTER);
								mainContentPanel.updateUI();
							}
						});
					}
				} else if (tab.equals("VM")) {
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							mainContentPanel.removeAll();
							mainContentPanel.add(new VMMainPanel(MainFrame.this), BorderLayout.CENTER);
							mainContentPanel.updateUI();
						}
					});
				} else if (tab.equals("Keystone")) {
					mainContentPanel.removeAll();
					mainContentPanel.add(new KeystonePanel(MainFrame.this), BorderLayout.CENTER);
					mainContentPanel.updateUI();
				} else if (tab.equals("Flavor")) {
					mainContentPanel.removeAll();
					mainContentPanel.add(new FlavorPanel(MainFrame.this), BorderLayout.CENTER);
					mainContentPanel.updateUI();
				} else if (tab.equals("Storage")) {
					mainContentPanel.removeAll();
					mainContentPanel.add(new StoragePanel(MainFrame.this), BorderLayout.CENTER);
					mainContentPanel.updateUI();
				} else if (tab.equals("Network")) {
					mainContentPanel.removeAll();
					mainContentPanel.add(new SDNPanel(MainFrame.this), BorderLayout.CENTER);
					mainContentPanel.updateUI();
				} else if (tab.equals("Setting")) {
					mainContentPanel.removeAll();
					mainContentPanel.add(new SettingPanel(MainFrame.this), BorderLayout.CENTER);
					mainContentPanel.updateUI();
				}
			}
		});
		ribbonTabbedPane.putClientProperty("type", "ribbonType");
		ribbonTabbedPane.setPreferredSize(new Dimension(1000, 140));
		ribbonPanel.add(ribbonTabbedPane, BorderLayout.CENTER);

		serverPanel = new JRibbonPanel();
		serverPanel.setLayout(new MigLayout("", "[][][][][][][][][][][][][][grow][]", "[grow][grow][]"));
		ribbonTabbedPane.addTab("Server", null, serverPanel, null);

		logoutButton = new JRibbonBigButton("Logout");
		logoutButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logout();
			}
		});

		rbnbgbtnAddServer = new JRibbonBigButton();
		rbnbgbtnAddServer.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/add.png")));
		rbnbgbtnAddServer.setText("Add Server");
		serverPanel.add(rbnbgbtnAddServer, "cell 0 0 1 3,growy");

		rbnbgbtnDeleteServer = new JRibbonBigButton();
		rbnbgbtnDeleteServer.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/delete.png")));
		rbnbgbtnDeleteServer.setText("Delete Server");
		serverPanel.add(rbnbgbtnDeleteServer, "cell 1 0 1 3,growy");
		logoutButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/logout.png")));
		logoutButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		logoutButton.setHorizontalTextPosition(SwingConstants.CENTER);
		serverPanel.add(logoutButton, "cell 14 0 1 3,growy");

		vmPanel = new JRibbonPanel();
		ribbonTabbedPane.addTab("VM", null, vmPanel, null);
		vmPanel.setLayout(new MigLayout("", "[][][][][][][][][][][][][][][grow][]", "[grow][grow][]"));

		launchButton = new JRibbonBigButton("Launch");
		launchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new LaunchInstanceDialog(MainFrame.this).setVisible(true);
			}
		});
		launchButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/launch.png")));
		launchButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		launchButton.setHorizontalTextPosition(SwingConstants.CENTER);
		vmPanel.add(launchButton, "cell 0 0 1 3,growy");

		pauseButton = new JRibbonButton("Pause");
		pauseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.action("from titan: nova pause");
			}
		});
		pauseButton.setHorizontalAlignment(SwingConstants.LEFT);
		pauseButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/control_pause.png")));
		vmPanel.add(pauseButton, "cell 2 0,growx");

		stopButton = new JRibbonButton("Stop");
		stopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.action("from titan: nova stop");
			}
		});
		stopButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/stop.png")));
		stopButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		stopButton.setHorizontalTextPosition(SwingConstants.CENTER);
		vmPanel.add(stopButton, "cell 1 0 1 3,growy");

		unpauseButton = new JRibbonButton("Unpause");
		unpauseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.action("from titan: nova unpause");
			}
		});
		unpauseButton.setHorizontalAlignment(SwingConstants.LEFT);
		unpauseButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/control_play.png")));

		suspendButton = new JRibbonButton("Suspend");
		suspendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.action("from titan: nova suspend");
			}
		});
		suspendButton.setHorizontalAlignment(SwingConstants.LEFT);
		suspendButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/disk.png")));
		vmPanel.add(suspendButton, "cell 3 0,growx");

		softRebootButton = new JRibbonButton("Soft reboot");
		softRebootButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.action("from titan: nova soft-reboot");
			}
		});
		softRebootButton.setHorizontalAlignment(SwingConstants.LEFT);
		softRebootButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/arrow_rotate_clockwise.png")));

		selectAllVMButton = new JRibbonButton("Select all");
		selectAllVMButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.selectAll();
			}
		});
		vmPanel.add(selectAllVMButton, "cell 4 0,growx");
		vmPanel.add(softRebootButton, "cell 9 0,growx");

		createMacroButton = new JRibbonButton("Create macro");
		createMacroButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/add.png")));
		createMacroButton.setHorizontalAlignment(SwingConstants.LEFT);

		setGroupNameButton = new JRibbonButton("Set group name");
		setGroupNameButton.setHorizontalAlignment(SwingConstants.LEFT);
		setGroupNameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.setGroupName();
			}
		});
		vmPanel.add(setGroupNameButton, "cell 10 0");
		vmPanel.add(createMacroButton, "cell 13 0,growx");

		ribbonSeparator_3 = new JRibbonSeparator();
		vmPanel.add(ribbonSeparator_3, "cell 14 0 1 3,growy");
		vmPanel.add(unpauseButton, "cell 2 1,growx");

		remoteButton = new JRibbonButton("Remote");
		remoteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.remote();
			}
		});
		remoteButton.setHorizontalAlignment(SwingConstants.LEFT);
		remoteButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/application_osx_terminal.png")));

		unselectAllButton = new JRibbonButton("Unselect all");
		unselectAllButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.unselectAll();
			}
		});
		vmPanel.add(unselectAllButton, "cell 4 1,growx");
		vmPanel.add(remoteButton, "cell 2 2,growx");

		performanceMeterButton = new JRibbonButton("Performance meter");
		performanceMeterButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/chart_curve.png")));
		performanceMeterButton.setHorizontalAlignment(SwingConstants.LEFT);
		vmPanel.add(performanceMeterButton, "cell 13 1,growx");

		resumeButton = new JRibbonButton("Resume");
		resumeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.action("from titan: nova resume");
			}
		});
		resumeButton.setHorizontalAlignment(SwingConstants.LEFT);
		resumeButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/drive_disk.png")));
		vmPanel.add(resumeButton, "cell 3 1,growx");

		deleteButton = new JRibbonBigButton("Delete");
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.action("from titan: nova delete");
			}
		});
		vmPanel.add(deleteButton, "cell 7 0 1 3,alignx center,growy");
		deleteButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/delete.png")));
		deleteButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		deleteButton.setHorizontalTextPosition(SwingConstants.CENTER);

		logButton = new JRibbonBigButton("Log");
		logButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.log();
			}
		});
		vmPanel.add(logButton, "cell 6 0 1 3,alignx center,growy");
		logButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/log.png")));
		logButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		logButton.setHorizontalTextPosition(SwingConstants.CENTER);

		hardRebootButton = new JRibbonButton("Hard reboot");
		hardRebootButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VMMainPanel vmMainPanel = (VMMainPanel) mainContentPanel.getComponent(0);
				vmMainPanel.action("from titan: nova hard-reboot");
			}
		});
		hardRebootButton.setHorizontalAlignment(SwingConstants.LEFT);
		hardRebootButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/arrow_undo.png")));

		ribbonSeparator = new JRibbonSeparator();
		vmPanel.add(ribbonSeparator, "cell 5 0 1 3,alignx center,growy");

		ribbonSeparator_1 = new JRibbonSeparator();
		vmPanel.add(ribbonSeparator_1, "cell 8 0 1 3,alignx center,growy");
		vmPanel.add(hardRebootButton, "cell 9 1,growx");

		ribbonSeparator_2 = new JRibbonSeparator();
		vmPanel.add(ribbonSeparator_2, "cell 11 0 1 3,grow");

		macroButton = new JRibbonBigButton("Macro");
		macroButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/code.png")));
		macroButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		macroButton.setHorizontalTextPosition(SwingConstants.CENTER);
		vmPanel.add(macroButton, "cell 12 0 1 3,growy");

		snapshotButton = new JRibbonButton("Snapshot");
		snapshotButton.setHorizontalAlignment(SwingConstants.LEFT);
		snapshotButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/application_cascade.png")));
		vmPanel.add(snapshotButton, "cell 3 2,growx");

		advanceButton = new JRibbonButton("Advance");
		advanceButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		advanceButton.setHorizontalAlignment(SwingConstants.LEFT);
		advanceButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/application_view_detail.png")));
		vmPanel.add(advanceButton, "cell 9 2,growx");

		executionMapButton = new JRibbonButton("Execution map");
		executionMapButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/chart_organisation.png")));
		executionMapButton.setHorizontalAlignment(SwingConstants.LEFT);
		vmPanel.add(executionMapButton, "cell 13 2,growx");

		keystonePanel = new JRibbonPanel();
		ribbonTabbedPane.addTab("Keystone", null, keystonePanel, null);
		keystonePanel.setLayout(new MigLayout("", "[][][][][][][][][][]", "[grow][][][][]"));

		addUserButton = new JRibbonBigButton("Add user");
		addUserButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeystonePanel keystonePanel = (KeystonePanel) mainContentPanel.getComponent(0);
				keystonePanel.addUser();
			}
		});
		addUserButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		addUserButton.setHorizontalTextPosition(SwingConstants.CENTER);
		addUserButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/addUser.png")));
		keystonePanel.add(addUserButton, "cell 0 0 1 3,growy");

		editUserButton = new JRibbonButton("Edit user");
		editUserButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		editUserButton.setHorizontalAlignment(SwingConstants.LEFT);
		editUserButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/pencil.png")));
		keystonePanel.add(editUserButton, "cell 1 0,growx");

		changePasswordButton = new JRibbonButton("Change password");
		keystonePanel.add(changePasswordButton, "cell 2 0");

		ribbonSeparator_4 = new JRibbonSeparator();
		keystonePanel.add(ribbonSeparator_4, "cell 3 0 1 3,growy");

		addRoleButton = new JRibbonBigButton("Add role");
		addRoleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeystonePanel keystonePanel = (KeystonePanel) mainContentPanel.getComponent(0);
				keystonePanel.addRole();
			}
		});
		addRoleButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		addRoleButton.setHorizontalTextPosition(SwingConstants.CENTER);
		addRoleButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/addRole.png")));
		keystonePanel.add(addRoleButton, "cell 4 0 1 3,growy");

		deleteUserButton = new JRibbonButton("Delete user");
		deleteUserButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		deleteUserButton.setHorizontalAlignment(SwingConstants.LEFT);
		deleteUserButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/cross.png")));

		editRoleButton = new JRibbonButton("Edit role");
		editRoleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		editRoleButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/pencil.png")));
		editRoleButton.setHorizontalAlignment(SwingConstants.LEFT);
		keystonePanel.add(editRoleButton, "cell 5 0,growx");

		ribbonSeparator_5 = new JRibbonSeparator();
		keystonePanel.add(ribbonSeparator_5, "cell 6 0 1 3,growy");

		createTenantButton = new JRibbonBigButton("Create tenant");
		createTenantButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeystonePanel keystonePanel = (KeystonePanel) mainContentPanel.getComponent(0);
				keystonePanel.createTenant();
			}
		});
		createTenantButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/add.png")));
		createTenantButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		createTenantButton.setHorizontalTextPosition(SwingConstants.CENTER);
		keystonePanel.add(createTenantButton, "cell 7 0 1 3,growy");

		deleteTenantButton = new JRibbonBigButton("Delete tenant");
		deleteTenantButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeystonePanel keystonePanel = (KeystonePanel) mainContentPanel.getComponent(0);
				keystonePanel.deleteTenant();
			}
		});
		deleteTenantButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/delete.png")));
		deleteTenantButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		deleteTenantButton.setHorizontalTextPosition(SwingConstants.CENTER);
		keystonePanel.add(deleteTenantButton, "cell 8 0 1 3,growy");
		keystonePanel.add(deleteUserButton, "cell 1 1,growx");

		detailUserButton = new JRibbonButton("Detail user");
		detailUserButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeystonePanel keystonePanel = (KeystonePanel) mainContentPanel.getComponent(0);
				keystonePanel.showUserDetail();
			}
		});
		detailUserButton.setHorizontalAlignment(SwingConstants.LEFT);
		detailUserButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/zoom.png")));
		keystonePanel.add(detailUserButton, "cell 1 2,growx");

		btnDeleteRole = new JRibbonButton("Delete role");
		btnDeleteRole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeystonePanel keystonePanel = (KeystonePanel) mainContentPanel.getComponent(0);
				keystonePanel.deleteRole();
			}
		});
		btnDeleteRole.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/cross.png")));
		keystonePanel.add(btnDeleteRole, "cell 5 1,growx");

		assignRoleButton = new JRibbonButton("Assign role");
		assignRoleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeystonePanel keystonePanel = (KeystonePanel) mainContentPanel.getComponent(0);
				keystonePanel.assignRole();
			}
		});
		assignRoleButton.setSelectedIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/user_add.png")));
		keystonePanel.add(assignRoleButton, "cell 5 2,growx");

		lblUser = new JRibbonLabel("user");
		keystonePanel.add(lblUser, "cell 0 3 2 1,growx");

		rbnlblRole = new JRibbonLabel("role");
		keystonePanel.add(rbnlblRole, "cell 4 3 2 1,growx");

		rbnlblTenant = new JRibbonLabel();
		rbnlblTenant.setText("tenant");
		keystonePanel.add(rbnlblTenant, "cell 7 3 2 1,growx");

		flavorPanel = new JRibbonPanel();
		ribbonTabbedPane.addTab("Flavor", null, flavorPanel, null);
		flavorPanel.setLayout(new MigLayout("", "[][]", "[grow][][][]"));

		rbnbgbtnCreateFlavor = new JRibbonBigButton();
		rbnbgbtnCreateFlavor.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/add.png")));
		rbnbgbtnCreateFlavor.setText("Create Flavor");
		flavorPanel.add(rbnbgbtnCreateFlavor, "cell 0 0 1 3,growy");

		btnDeleteFlavor = new JRibbonBigButton("Delete Flavor");
		btnDeleteFlavor.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/delete.png")));
		flavorPanel.add(btnDeleteFlavor, "cell 1 0 1 3,growy");

		storagePanel = new JRibbonPanel();
		ribbonTabbedPane.addTab("Storage", null, storagePanel, null);
		storagePanel.setLayout(new MigLayout("", "[][][][][][]", "[grow][][][]"));

		uploadImageButton = new JRibbonBigButton("Upload");
		uploadImageButton.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/add.png")));
		uploadImageButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		uploadImageButton.setHorizontalTextPosition(SwingConstants.CENTER);
		storagePanel.add(uploadImageButton, "cell 0 0 1 3, growy");

		btnDelete = new JRibbonButton("Delete");
		btnDelete.setHorizontalAlignment(SwingConstants.LEFT);
		btnDelete.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/cross.png")));
		storagePanel.add(btnDelete, "cell 1 0,growx");

		ribbonSeparator_6 = new JRibbonSeparator();
		storagePanel.add(ribbonSeparator_6, "cell 2 0 1 3,grow");

		btnCreateVolume = new JRibbonBigButton("Create volume");
		btnCreateVolume.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/add.png")));
		btnCreateVolume.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnCreateVolume.setHorizontalTextPosition(SwingConstants.CENTER);
		storagePanel.add(btnCreateVolume, "cell 3 0 1 3,growy");

		btnDelete_1 = new JRibbonButton("Delete");
		btnDelete_1.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/cross.png")));
		btnDelete_1.setHorizontalAlignment(SwingConstants.LEFT);
		storagePanel.add(btnDelete_1, "cell 4 0,growx");

		btnAttach = new JRibbonButton("Attach to vm");
		btnAttach.setHorizontalAlignment(SwingConstants.LEFT);
		btnAttach.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/attach.png")));
		storagePanel.add(btnAttach, "cell 5 0,growx");

		btnChangeName = new JRibbonButton("Change name");
		btnChangeName.setHorizontalAlignment(SwingConstants.LEFT);
		btnChangeName.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/pencil.png")));
		storagePanel.add(btnChangeName, "cell 1 1,growx");

		btnDetail = new JRibbonButton("Detail");
		btnDetail.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/zoom.png")));
		btnDetail.setHorizontalAlignment(SwingConstants.LEFT);
		storagePanel.add(btnDetail, "cell 4 1,growx");

		btnDetachToVm = new JRibbonButton("Detach to vm");
		btnDetachToVm.setHorizontalAlignment(SwingConstants.LEFT);
		btnDetachToVm.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/delete.png")));
		storagePanel.add(btnDetachToVm, "cell 5 1,growx");

		btnPublicprivate = new JRibbonButton("public/private");
		btnPublicprivate.setHorizontalAlignment(SwingConstants.LEFT);
		btnPublicprivate.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/tick.png")));
		storagePanel.add(btnPublicprivate, "cell 1 2,growx");

		btnAddVolumeType = new JRibbonButton("Add volume type");
		btnAddVolumeType.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/add.png")));
		btnAddVolumeType.setHorizontalAlignment(SwingConstants.LEFT);
		storagePanel.add(btnAddVolumeType, "cell 4 2");

		btnDeleteVolumeType = new JRibbonButton("Delete volume type");
		btnDeleteVolumeType.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/cross.png")));
		btnDeleteVolumeType.setHorizontalAlignment(SwingConstants.LEFT);
		storagePanel.add(btnDeleteVolumeType, "cell 5 2");

		rbnlblImage = new JRibbonLabel();
		rbnlblImage.setText("Image");
		storagePanel.add(rbnlblImage, "cell 0 3 2 1,growx");

		rbnlblVolume = new JRibbonLabel();
		rbnlblVolume.setText("Volume");
		storagePanel.add(rbnlblVolume, "cell 3 3 3 1,growx");

		networkPanel = new JRibbonPanel();
		ribbonTabbedPane.addTab("Network", null, networkPanel, null);

		settingPanel = new JRibbonPanel();
		ribbonTabbedPane.addTab("Setting", null, settingPanel, null);
		settingPanel.setLayout(new MigLayout("", "[][][]", "[grow][grow][]"));

		rbnbgbtnSystemSetting = new JRibbonBigButton();
		rbnbgbtnSystemSetting.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/systemSetting.png")));
		rbnbgbtnSystemSetting.setText("System Setting");
		settingPanel.add(rbnbgbtnSystemSetting, "cell 0 0 1 3,growy");

		rbnbgbtnDatabase = new JRibbonBigButton();
		rbnbgbtnDatabase.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/ribbon/database.png")));
		rbnbgbtnDatabase.setText("Database");
		settingPanel.add(rbnbgbtnDatabase, "cell 1 0 1 3,growy");

		rbnbtnAddGroup = new JRibbonButton();
		rbnbtnAddGroup.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/add.png")));
		rbnbtnAddGroup.setText("Add Group");
		settingPanel.add(rbnbtnAddGroup, "cell 2 0");

		rbnbtnEditGroup = new JRibbonButton();
		rbnbtnEditGroup.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/pencil.png")));
		rbnbtnEditGroup.setText("Edit Group");
		settingPanel.add(rbnbtnEditGroup, "cell 2 1");

		rbnbtnDeleteGroup = new JRibbonButton();
		rbnbtnDeleteGroup.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/cross.png")));
		rbnbtnDeleteGroup.setText("Delete Group");
		settingPanel.add(rbnbtnDeleteGroup, "cell 2 2");

		logoPanel = new JRibbonPanel();
		ribbonPanel.add(logoPanel, BorderLayout.WEST);
		logoPanel.setLayout(new BorderLayout(0, 0));
		logoPanel.add(logoLabel, BorderLayout.CENTER);

		setLocationRelativeTo(null);

		new Thread(new TitanServerUpdateThread()).start();
	}

	public void updateComputeTree() {
		try {
			serverRoot.children.removeAllElements();
			//		TitanServerDefinition serverDefinition = new TitanServerDefinition();
			//		serverDefinition.id = "";
			//		serverDefinition.ip = Global.primaryServerIP;
			//
			//		Command command = new Command();
			//		command.command = "getID";
			//		ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
			//		if (r != null) {
			//			serverDefinition.id = r.message;
			//		}
			//
			//		ServerTreeNode server = new ServerTreeNode(serverDefinition);
			//		server.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/server.png")));
			//		serverRoot.children.add(server);
			//
			//		for (int x = 0; x < TitanSetting.getInstance().titanServers.size(); x++) {
			//			server = new ServerTreeNode(TitanSetting.getInstance().titanServers.get(x));
			//			server.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/server.png")));
			//			serverRoot.children.add(server);
			//		}

			Command command = new Command();
			command.command = "from titan: nova host-list";
			ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
			HttpResult httpResult = (HttpResult) r.map.get("result");
			JSONArray hosts = JSONObject.fromObject(httpResult.content).getJSONArray("hosts");
			for (int x = 0; x < hosts.size(); x++) {
				JSONObject host = hosts.getJSONObject(x);
				String service = host.getString("service");
				if (service.equals("compute")) {
					String host_name = host.getString("host_name");
					ZoneTextTreeNode zoneNode = new ZoneTextTreeNode(host_name, new ImageIcon(MainFrame.class.getResource("/com/titan/image/zoneTree/nova.png")));
					serverRoot.children.add(zoneNode);
				}
			}
			computeTreeModel.reload();
			((FilterTreeModel) serverTree.getModel()).reload();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void updateZoneTree() {
		try {
			zoneRoot.children.removeAllElements();

			Command command = new Command();
			command.command = "from titan: nova availability-zone-list";
			ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
			HttpResult httpResult = (HttpResult) r.map.get("result");
			//		System.out.println(httpResult.content);

			JSONArray top = JSONObject.fromObject(httpResult.content).getJSONArray("availabilityZoneInfo");
			for (int x = 0; x < top.size(); x++) {
				JSONObject zone = top.getJSONObject(x);
				JSONObject hosts = zone.getJSONObject("hosts");
				String zoneName = zone.getString("zoneName");
				ZoneTextTreeNode zoneNode = new ZoneTextTreeNode(zoneName);
				if (zoneName.equals("nova")) {
					zoneNode.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/zoneTree/nova.png")));
				} else {
					zoneNode.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/server.png")));
				}
				zoneRoot.children.add(zoneNode);
				//		System.out.println(hosts);

				Iterator<String> hostKeys = hosts.keys();
				while (hostKeys.hasNext()) {
					String hostStr = (String) hostKeys.next();
					ZoneTextTreeNode hostNode = new ZoneTextTreeNode(hostStr);
					hostNode.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/server.png")));
					zoneNode.children.add(hostNode);

					JSONObject hostObj = hosts.getJSONObject(hostStr);
					Iterator<String> keys = hostObj.keys();
					while (keys.hasNext()) {
						String key = (String) keys.next();
						if (hostObj.get(key) instanceof JSONObject) {
							ZoneTextTreeNode node = new ZoneTextTreeNode(key);
							if (zoneName.equals("nova")) {
								node.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/zoneTree/vm.png")));
							} else {
								node.setIcon(new ImageIcon(MainFrame.class.getResource("/com/titan/image/famfamfam/box.png")));
							}
							hostNode.children.add(node);
						}
					}
				}
			}
			zoneTreeModel.reload();
			((FilterTreeModel) zoneTree.getModel()).reload();
			CommonLib.expandAll(zoneTree, true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void logout() {
		setVisible(false);
		Titan frame = new Titan();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	@Override
	public void handleAbout(ApplicationEvent arg0) {

	}

	@Override
	public void handleOpenApplication(ApplicationEvent arg0) {

	}

	@Override
	public void handleOpenFile(ApplicationEvent arg0) {

	}

	@Override
	public void handlePreferences(ApplicationEvent arg0) {

	}

	@Override
	public void handlePrintFile(ApplicationEvent arg0) {

	}

	@Override
	public void handleQuit(ApplicationEvent arg0) {
		handleQuit();
		System.exit(0);
	}

	@Override
	public void handleReOpenApplication(ApplicationEvent arg0) {

	}

	private void handleQuit() {
		TitanSetting.getInstance().mainframeDivX = MainFrame.this.splitPane.getDividerLocation();
		TitanSetting.getInstance().x = MainFrame.this.getX();
		TitanSetting.getInstance().y = MainFrame.this.getY();
		TitanSetting.getInstance().width = MainFrame.this.getWidth();
		TitanSetting.getInstance().height = MainFrame.this.getHeight();
		TitanSetting.getInstance().save();
	}

}
