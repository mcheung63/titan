package com.titan.mainframe;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.tree.TreeCellRenderer;

public class ZoneTreeRenderer extends JLabel implements TreeCellRenderer {
	LineBorder border = new LineBorder(new Color(100, 188, 255));
	EmptyBorder emptyBorder = new EmptyBorder(1, 1, 1, 1);

	public ZoneTreeRenderer() {
		this.setOpaque(true);
	}

	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		ZoneMutableTreeNode node = (ZoneMutableTreeNode) value;
		this.setIcon(node.getIcon());
		this.setText(node.toString());
		if (sel) {
			this.setBorder(border);
			this.setBackground(UIManager.getColor("Tree.selectionBackground"));
		} else {
			this.setBorder(emptyBorder);
			this.setBackground(UIManager.getColor("Tree.background"));
		}
		return this;
	}

	public Dimension getPreferredSize() {
		Dimension d = super.getPreferredSize();
		d.width += 5;
		return d;
	}
}
