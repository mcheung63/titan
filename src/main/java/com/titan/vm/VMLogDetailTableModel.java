package com.titan.vm;

import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import net.sf.json.JSONObject;

import com.titan.TitanCommonLib;
import com.titan.communication.CommunicateLib;
import com.titancommon.Command;
import com.titancommon.ReturnCommand;
import com.titanserver.table.RestServerVmStat;

public class VMLogDetailTableModel extends DefaultTableModel implements Runnable {
	String instanceId;
	String columnNames[] = { "pid", "name", "total cpu", "resident mem", "vsize", "command" };
	Vector<String> pid = new Vector<String>();
	Vector<String> name = new Vector<String>();
	Vector<String> totalCpu = new Vector<String>();
	Vector<String> memResident = new Vector<String>();
	Vector<String> memVsize = new Vector<String>();
	Vector<String> commandLine = new Vector<String>();

	Vector<Vector<String>> data = new Vector<Vector<String>>();

	public VMLogDetailTableModel() {
		data.add(pid);
		data.add(name);
		data.add(totalCpu);
		data.add(memResident);
		data.add(memVsize);
		data.add(commandLine);
	}

	public String getColumnName(int column) {
		return columnNames[column];
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		if (pid == null) {
			return 0;
		}
		return pid.size();
	}

	public void setValueAt(Object aValue, int row, int column) {
		fireTableCellUpdated(row, column);
	}

	public Object getValueAt(final int row, int column) {
		return data.get(column).get(row);
	}

	public boolean isCellEditable(int row, int column) {
		return false;
	}

	public Class getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public void run() {
		pid.clear();
		name.clear();
		totalCpu.clear();
		memResident.clear();
		memVsize.clear();
		commandLine.clear();

		if (instanceId != null) {
			try {
				Command command = new Command();
				command.command = "getVMStat";
				command.parameters.add(instanceId);
				command.parameters.add(new Date());
				ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
				if (r != null && r.map.get("result") != null) {
					RestServerVmStat restServerVmStat = (RestServerVmStat) r.map.get("result");
					JSONObject json = JSONObject.fromObject(restServerVmStat.getMessage()).getJSONObject("result").getJSONObject("processes");
					Iterator<String> keys = json.keys();
					//					for (int x = 0; x < json.size(); x++) {
					while (keys.hasNext()) {
						String pidStr = keys.next();
						JSONObject element = JSONObject.fromObject(json.get(pidStr));
						addValue(pid, element, "pid");
						addValue(name, element.getJSONObject("procState"), "name");
						addValue(totalCpu, element.getJSONObject("procCpu"), "total");
						addValue(memResident, element.getJSONObject("procMem"), "resident");
						addValue(memVsize, element.getJSONObject("procMem"), "vsize");
						addValue(commandLine, element, "commandLine");
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		fireTableDataChanged();
	}

	private void addValue(Vector<String> v, JSONObject value, String key) {
		try {
			v.add(value.getString(key));
		} catch (Exception ex) {
			v.add(null);
		}
	}

	public int getColumnIndex(String columnName) {
		for (int x = 0; x < columnNames.length; x++) {
			if (columnNames[x].equals(columnName)) {
				return x;
			}
		}
		return -1;
	}
}
