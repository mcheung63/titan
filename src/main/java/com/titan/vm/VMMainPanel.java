package com.titan.vm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableRowSorter;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.peterswing.advancedswing.jprogressbardialog.JProgressBarDialog;
import com.peterswing.advancedswing.searchtextfield.JSearchTextField;
import com.titan.TitanCommonLib;
import com.titan.TitanSetting;
import com.titan.communication.CommunicateLib;
import com.titan.instancepanel.InstancePanel;
import com.titan.instancepanel.MonitorDialog;
import com.titan.mainframe.MainFrame;
import com.titan.vm.vmdialog.VMDialog;
import com.titancommon.Command;
import com.titancommon.HttpResult;
import com.titancommon.ReturnCommand;
import com.titanserver.table.InstanceType;

public class VMMainPanel extends JPanel implements Runnable {
	MainFrame mainframe;
	JSlider slider = new JSlider();
	VMIconPanel iconPanel = new VMIconPanel(this);
	JLabel colLabel = new JLabel("");
	ButtonGroup group1 = new ButtonGroup();
	ButtonGroup chartButtonGroup = new ButtonGroup();
	private JToggleButton ganttViewButton;
	private JToggleButton deltailViewButton;
	JScrollPane mainScrollPane = new JScrollPane();
	VMGanttPanel vmGanttPanel;
	private JComboBox sortComboBox;
	JSONObject selectedVM = null;
	private JTable propertyTable;
	private JSplitPane splitPane;
	PropertyTableModel propertyTableModel = new PropertyTableModel();
	private JSearchTextField searchPropertyTextField;
	TableRowSorter<PropertyTableModel> propertyTableRowSorter;
	private JRadioButton chartRadioButton;
	private JRadioButton numberRadioButton;
	private JSearchTextField searchTextField;
	boolean isUpdatePropertyTableThreadRunning = false;
	boolean isUpdatePropertyTableThreadTrigger = false;
	public JComboBox groupComboBox;
	public static HashMap<String, MonitorDialog> monitorDialogs = new HashMap<String, MonitorDialog>();

	public VMMainPanel(final MainFrame mainFrame) {
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentHidden(ComponentEvent e) {
				System.out.println("componentHidden");
			}
		});
		this.mainframe = mainFrame;
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));

		JToolBar toolBar = new JToolBar();
		panel.add(toolBar);

		JToggleButton iconViewButton = new JToggleButton("");
		iconViewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainScrollPane.setViewportView(iconPanel);
			}
		});
		iconViewButton.setSelected(true);
		iconViewButton.setIcon(new ImageIcon(VMMainPanel.class.getResource("/com/titan/image/famfamfam/color_swatch.png")));
		toolBar.add(iconViewButton);
		group1.add(iconViewButton);

		ganttViewButton = new JToggleButton("");
		ganttViewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (vmGanttPanel == null) {
					vmGanttPanel = new VMGanttPanel();
				}
				mainScrollPane.setViewportView(vmGanttPanel);
			}
		});
		ganttViewButton.setIcon(new ImageIcon(VMMainPanel.class.getResource("/com/titan/image/famfamfam/text_list_bullets.png")));
		toolBar.add(ganttViewButton);
		group1.add(ganttViewButton);

		deltailViewButton = new JToggleButton("");
		deltailViewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainScrollPane.setViewportView(new InstancePanel(mainframe));
			}
		});
		deltailViewButton.setIcon(new ImageIcon(VMMainPanel.class.getResource("/com/titan/image/famfamfam/text_align_justify.png")));
		toolBar.add(deltailViewButton);
		group1.add(deltailViewButton);

		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refresh(true);
			}
		});
		btnRefresh.setIcon(new ImageIcon(VMMainPanel.class.getResource("/com/titan/image/famfamfam/arrow_rotate_clockwise.png")));
		toolBar.add(btnRefresh);

		searchTextField = new JSearchTextField();
		searchTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				refresh(false);
			}
		});
		searchTextField.setMaximumSize(new Dimension(100, 20));
		toolBar.add(searchTextField);

		slider.setMaximumSize(new Dimension(120, 20));
		slider.setMinimum(5);
		slider.setMaximum(20);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				refresh(false);
			}
		});
		toolBar.add(slider);

		slider.setValue(TitanSetting.getInstance().maxVMColumnCount);

		colLabel.setText(TitanSetting.getInstance().maxVMColumnCount + " columns");
		toolBar.add(colLabel);

		sortComboBox = new JComboBox(new String[] { "name", "cpu", "memory", "disk" });
		sortComboBox.setMaximumSize(new Dimension(100, 20));
		toolBar.add(sortComboBox);

		groupComboBox = new JComboBox();
		groupComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("s=" + groupComboBox.getSelectedItem());
			}
		});
		groupComboBox.setMaximumSize(new Dimension(100, 20));
		toolBar.add(groupComboBox);

		chartRadioButton = new JRadioButton("Chart");
		chartRadioButton.setVisible(false);
		chartButtonGroup.add(chartRadioButton);
		chartRadioButton.setSelected(true);
		toolBar.add(chartRadioButton);

		numberRadioButton = new JRadioButton("Number");
		numberRadioButton.setVisible(false);
		chartButtonGroup.add(numberRadioButton);
		toolBar.add(numberRadioButton);

		mainScrollPane.getVerticalScrollBar().setUnitIncrement(20);
		mainScrollPane.setViewportView(iconPanel);

		JPanel propertyPanel = new JPanel();
		propertyPanel.setLayout(new BorderLayout(0, 0));

		JScrollPane propertyScrollPane = new JScrollPane();
		propertyPanel.add(propertyScrollPane, BorderLayout.CENTER);

		propertyTable = new JTable();
		propertyTableModel.addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {
				if (e.getType() == TableModelEvent.UPDATE && e.getColumn() >= 0) {
					Property property = (Property) propertyTableModel.getValueAt(e.getFirstRow(), e.getColumn());
					if (property.name.equals("name")) {
						Command command = new Command();
						command.command = "from titan: nova rename";
						HashMap<String, String> parameters = new HashMap<String, String>();
						parameters.put("$InstanceId", TitanCommonLib.getJSONString(selectedVM, "id", null));
						parameters.put("$name", property.value);
						command.parameters.add(parameters);
						ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
					} else if (property.name.equals("type") || property.name.equals("group")) {
						Command command = new Command();
						command.command = "set instance type";
						HashMap<String, String> parameters = new HashMap<String, String>();
						parameters.put("instanceId", TitanCommonLib.getJSONString(selectedVM, "id", null));
						parameters.put("name", property.name);
						parameters.put("value", property.value);
						command.parameters.add(parameters);
						ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
					}
				}
			}
		});
		propertyTable.setModel(propertyTableModel);
		propertyTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		propertyTable.setDefaultRenderer(Property.class, new PropertyTableCellRenderer());
		propertyTable.setDefaultEditor(Property.class, new PropertyTableEditor());
		//		propertyTable.getColumnModel().getColumn(3).setCellEditor(new PropertyTableEditor());
		propertyTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		propertyTableRowSorter = new TableRowSorter<PropertyTableModel>(propertyTableModel);
		propertyTable.setRowSorter(propertyTableRowSorter);
		RowFilter<PropertyTableModel, Integer> filter = new RowFilter<PropertyTableModel, Integer>() {
			@Override
			public boolean include(javax.swing.RowFilter.Entry<? extends PropertyTableModel, ? extends Integer> entry) {
				PropertyTableModel model = entry.getModel();
				Property property = (Property) model.getValueAt(entry.getIdentifier(), 0);
				if (searchPropertyTextField.getText().trim().equals("")) {
					return true;
				} else if (property.isData && property.name.toLowerCase().contains(searchPropertyTextField.getText().trim().toLowerCase())) {
					return true;
				} else if (!property.isData) {
					for (Property p : model.data) {
						if (p.isData && property.type.equals(p.type) && p.name.toLowerCase().contains(searchPropertyTextField.getText().trim().toLowerCase())) {
							return true;
						}
					}
					return false;
				} else {
					return false;
				}
			}
		};
		propertyTableRowSorter.setRowFilter(filter);
		propertyScrollPane.setViewportView(propertyTable);

		splitPane = new JSplitPane();
		splitPane.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				if (splitPane.getDividerLocation() > 0) {
					TitanSetting.getInstance().vmDivX = splitPane.getDividerLocation();
					TitanSetting.getInstance().save();
				}
			}
		});
		splitPane.setOneTouchExpandable(true);
		add(splitPane, BorderLayout.CENTER);
		splitPane.add(mainScrollPane, JSplitPane.LEFT);
		splitPane.add(propertyPanel, JSplitPane.RIGHT);
		splitPane.setDividerLocation(TitanSetting.getInstance().vmDivX);

		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_1.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		propertyPanel.add(panel_1, BorderLayout.NORTH);

		searchPropertyTextField = new JSearchTextField();
		searchPropertyTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				//				propertyTableModel.filter(searchPropertyTextField.getText());
				propertyTableModel.fireTableStructureChanged();
			}
		});
		searchPropertyTextField.setPreferredSize(new Dimension(80, 20));
		panel_1.add(searchPropertyTextField);
		splitPane.setResizeWeight(0.7d);

		initPropertyTableModel();
		propertyTable.getColumnModel().getColumn(0).setPreferredWidth(20);
		propertyTable.getColumnModel().getColumn(1).setPreferredWidth(200);
		propertyTable.getColumnModel().getColumn(2).setPreferredWidth(200);

		refresh(true);
	}

	public void remote() {
		String instanceName = TitanCommonLib.getJSONString(selectedVM, "OS-EXT-SRV-ATTR:instance_name", null);
		String name = TitanCommonLib.getJSONString(selectedVM, "name", null);
		MonitorDialog monitorDialog = monitorDialogs.get(instanceName);
		if (monitorDialog == null || !monitorDialog.isVisible()) {
			monitorDialog = new MonitorDialog(name, instanceName);
			monitorDialogs.put(instanceName, monitorDialog);
		}
		monitorDialog.setVisible(true);
	}

	private void initPropertyTableModel() {
		propertyTableModel.data.add(new Property("instance", "", "", false, false, "000"));
		propertyTableModel.data.add(new Property("instance", "name", "", true, true, "001"));
		propertyTableModel.data.add(new Property("instance", "type", "", true, true, "002"));
		propertyTableModel.data.add(new Property("instance", "group", "", true, true, "003"));
		propertyTableModel.data.add(new Property("instance", "ip address", "", true, false, "004"));
		propertyTableModel.data.add(new Property("instance", "mac addr", "", true, false, "004"));
		propertyTableModel.data.add(new Property("instance", "id", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "status", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "updated", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "hostId", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "OS-EXT-SRV-ATTR:host", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "links", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "image", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "OS-EXT-STS:vm_state", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "OS-EXT-SRV-ATTR:instance_name", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "OS-SRV-USG:launched_at", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "OS-EXT-SRV-ATTR:hypervisor_hostname", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "flavor", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "OS-EXT-AZ:availability_zone", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "user_id", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "created", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "tenant_id", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "OS-DCF:diskConfig", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "os-extended-volumes:volumes_attached", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "accessIPv4", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "accessIPv6", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "OS-EXT-STS:power_state", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "config_drive", "", true, false, "100"));
		propertyTableModel.data.add(new Property("instance", "metadata", "", true, false, "100"));

		propertyTableModel.data.add(new Property("diagnostics", "", "", false, false, "200"));
		propertyTableModel.data.add(new Property("diagnostics", "cpu0_time", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "hdd_errors", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "hdd_read", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "hdd_read_req", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "hdd_write", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "hdd_write_req", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "memory", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vda_errors", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vda_read", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vda_read_req", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vda_write", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vda_write_req", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vnet1_rx", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vnet1_rx_drop", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vnet1_rx_errors", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vnet1_rx_packets", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vnet1_tx", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vnet1_tx_drop", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vnet1_tx_errors", "", true, false, "300"));
		propertyTableModel.data.add(new Property("diagnostics", "vnet1_tx_packets", "", true, false, "300"));

		Collections.sort(propertyTableModel.data);

		propertyTableModel.fireTableStructureChanged();
	}

	void refresh(final boolean refreshServerList) {
		JProgressBarDialog d = new JProgressBarDialog(mainframe, true);
		d.thread = new Thread() {
			public void run() {
				int maxVMColumnCount = (int) slider.getValue();
				iconPanel.init(searchTextField.getText(), maxVMColumnCount, refreshServerList);
			}
		};
		d.progressBar.setIndeterminate(true);
		d.progressBar.setStringPainted(true);
		d.progressBar.setString("Refreshing");
		d.setVisible(true);
	}

	public void action(final String cmdStr) {
		VMPanel vmPanel = iconPanel;
		Vector<JSONObject> vms = vmPanel.getSelectedVM();

		if (vms.size() == 0) {
			JOptionPane.showMessageDialog(VMMainPanel.this.mainframe, "Please select vm first", "Warning", JOptionPane.WARNING_MESSAGE);
			return;
		}
		final VMDialog vmDialog = new VMDialog(mainframe);
		vmDialog.vmDialogTableModel.data.clear();
		for (JSONObject json : vms) {
			String instanceId = TitanCommonLib.getJSONString(json, "id", null);
			String name = TitanCommonLib.getJSONString(json, "name", null);
			Command command = new Command();
			command.command = "get instance type";
			command.parameters.add(instanceId);
			ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
			InstanceType instanceType = (InstanceType) r.map.get("result");
			String vmType = null;
			if (instanceType != null) {
				vmType = instanceType.getType();
			}
			vmDialog.vmDialogTableModel.add(instanceId, name, vmType);
		}
		vmDialog.thread = new Thread() {
			public void run() {
				for (int x = 0; x < vmDialog.vmDialogTableModel.getRowCount(); x++) {
					if (vmDialog.stopTrigger) {
						break;
					}
					Command cmd = new Command();
					cmd.command = cmdStr;
					HashMap<String, String> parameters = new HashMap<String, String>();
					String instanceId = (String) vmDialog.vmDialogTableModel.getValueAt(x, 0);
					parameters.put("$InstanceId", instanceId);
					vmDialog.vmDialogTableModel.updateStatus(instanceId, "running");
					cmd.parameters.add(parameters);
					ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), cmd);
					HttpResult httpResult = (HttpResult) r.map.get("result");
					vmDialog.vmDialogTableModel.updateStatus(instanceId, "done");
				}
				vmDialog.cancelButton.setText("Close");
				refresh(true);
			}
		};
		vmDialog.setVisible(true);
	}

	@Override
	public void run() {
		try {
			isUpdatePropertyTableThreadTrigger = false;
			isUpdatePropertyTableThreadRunning = true;
			for (Property p : propertyTableModel.data) {
				if (p.name.equals("ip address")) {
					try {
						JSONArray arr = selectedVM.getJSONObject("addresses").getJSONArray("vmnet");
						JSONObject temp = arr.getJSONObject(0);
						propertyTableModel.changeValue(p.name, temp.getString("addr"));
					} catch (Exception ex) {
					}
				} else if (p.name.equals("mac addr")) {
					try {
						JSONArray arr = selectedVM.getJSONObject("addresses").getJSONArray("vmnet");
						JSONObject temp = arr.getJSONObject(0);
						propertyTableModel.changeValue(p.name, temp.getString("OS-EXT-IPS-MAC:mac_addr"));
					} catch (Exception ex) {
					}
				} else {
					propertyTableModel.changeValue(p.name, TitanCommonLib.getJSONString(selectedVM, p.name, null));
				}
			}

			Command command = new Command();
			command.command = "from titan: nova diagnostics";
			HashMap<String, String> parameters = new HashMap<String, String>();
			String instanceId = TitanCommonLib.getJSONString(selectedVM, "id", null);
			parameters.put("$InstanceId", instanceId);
			command.parameters.add(parameters);
			ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
			HttpResult json = (HttpResult) r.map.get("result");
			if (json.content != null && !json.content.equals("")) {
				JSONObject jsonObject = JSONObject.fromObject(json.content);

				for (Property p : propertyTableModel.data) {
					if (isUpdatePropertyTableThreadTrigger) {
						isUpdatePropertyTableThreadRunning = false;
						return;
					}
					String value = TitanCommonLib.getJSONString(jsonObject, p.name, null);
					if (value != null) {
						propertyTableModel.changeValue(p.name, value);
					}
				}
			}

			command = new Command();
			command.command = "get instance type";
			command.parameters.add(instanceId);
			r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
			InstanceType instanceType = (InstanceType) r.map.get("result");
			if (instanceType != null) {
				propertyTableModel.changeValue("type", instanceType.getType());
				propertyTableModel.changeValue("group", instanceType.getGroupName());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		isUpdatePropertyTableThreadRunning = false;
	}

	public void log() {
		String instanceId = TitanCommonLib.getJSONString(selectedVM, "id", null);
		String instanceName = TitanCommonLib.getJSONString(selectedVM, "name", null);
		VMLogDialog dialog = new VMLogDialog(mainframe, instanceId, instanceName);
		dialog.setVisible(true);
	}

	public void setGroupName() {
		VMPanel vmPanel = iconPanel;
		Vector<JSONObject> vms = vmPanel.getSelectedVM();

		if (vms.size() == 0) {
			JOptionPane.showMessageDialog(VMMainPanel.this.mainframe, "Please select vm first", "Warning", JOptionPane.WARNING_MESSAGE);
			return;
		}
		final String groupName = JOptionPane.showInputDialog("Please input group name?");
		if (groupName == null) {
			return;
		}

		final VMDialog vmDialog = new VMDialog(mainframe);
		vmDialog.vmDialogTableModel.data.clear();
		for (JSONObject json : vms) {
			String instanceId = TitanCommonLib.getJSONString(json, "id", null);
			String name = TitanCommonLib.getJSONString(json, "name", null);
			String vmType = "unknown";
			vmDialog.vmDialogTableModel.add(instanceId, name, vmType);
		}
		vmDialog.thread = new Thread() {
			public void run() {
				for (int x = 0; x < vmDialog.vmDialogTableModel.getRowCount(); x++) {
					if (vmDialog.stopTrigger) {
						break;
					}
					Command command = new Command();
					command.command = "set instance type";
					HashMap<String, String> parameters = new HashMap<String, String>();
					String instanceId = (String) vmDialog.vmDialogTableModel.getValueAt(x, 0);
					parameters.put("instanceId", instanceId);
					parameters.put("name", "group");
					parameters.put("value", groupName);
					command.parameters.add(parameters);
					ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
					vmDialog.vmDialogTableModel.updateStatus(instanceId, "done");
				}
				vmDialog.cancelButton.setText("Close");
				refresh(true);
				new Thread(VMMainPanel.this).start();
			}
		};
		vmDialog.setVisible(true);
	}

	public void selectAll() {
		iconPanel.selectAll();
	}

	public void unselectAll() {
		iconPanel.unselectAll();
	}
}
