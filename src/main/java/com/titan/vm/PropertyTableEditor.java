package com.titan.vm;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;

import com.titan.Global;
import com.titan.OSType;

public class PropertyTableEditor implements TableCellEditor {
	private CellEditorListener cellEditorListener = null;
	Property originalProperty;

	JTextField textField = new JTextField();
	JComboBox<OSType> osTypeComboBox = new JComboBox<OSType>(Global.osTypes);

	@Override
	public Component getTableCellEditorComponent(JTable table, Object obj, boolean isSelected, int row, int column) {
		Property property = (Property) obj;
		originalProperty = property;

		if (property.name.equals("name")) {
			textField.setText(property.value);
			return textField;
		} else if (property.name.equals("type")) {
			osTypeComboBox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					stopCellEditing();
				}
			});
			osTypeComboBox.setSelectedItem(property.value);
			return osTypeComboBox;
		} else {
			textField.setText(property.value);
			return textField;
		}
	}

	@Override
	public Object getCellEditorValue() {
		if (originalProperty.name.equals("name")) {
			originalProperty.value = textField.getText();
			return originalProperty;
		} else if (originalProperty.name.equals("type")) {
			OSType osType = (OSType) osTypeComboBox.getSelectedItem();
			if (osType != null) {
				originalProperty.value = osType.name;
			}
			return originalProperty;
		} else {
			originalProperty.value = textField.getText();
			return originalProperty;
		}
	}

	@Override
	public boolean isCellEditable(EventObject e) {
		return true;
	}

	@Override
	public boolean shouldSelectCell(EventObject e) {
		return true;
	}

	@Override
	public boolean stopCellEditing() {
		if (cellEditorListener != null) {
			cellEditorListener.editingStopped(new ChangeEvent(this));
		}
		return true;
	}

	@Override
	public void cancelCellEditing() {
		if (cellEditorListener != null) {
			cellEditorListener.editingCanceled(new ChangeEvent(this));
		}
	}

	@Override
	public void addCellEditorListener(CellEditorListener celleditorlistener) {
		cellEditorListener = celleditorlistener;
	}

	@Override
	public void removeCellEditorListener(CellEditorListener celleditorlistener) {
		if (this.cellEditorListener == celleditorlistener) {
			cellEditorListener = null;
		}
	}
}