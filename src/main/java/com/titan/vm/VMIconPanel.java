package com.titan.vm;

import java.awt.Component;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import net.miginfocom.swing.MigLayout;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import com.titan.TitanCommonLib;
import com.titan.TitanSetting;
import com.titan.communication.CommunicateLib;
import com.titancommon.Command;
import com.titancommon.HttpResult;
import com.titancommon.ReturnCommand;
import com.titanserver.table.InstanceType;

public class VMIconPanel extends JPanel implements Runnable, VMPanel {
	JSONArray servers;
	VMMainPanel vmMainPanel;
	Vector<VMIcon> vmIcons = new Vector<VMIcon>();
	String searchPattern;
	int maxVMColumnCount;
	List<InstanceType> instanceTypes;

	public VMIconPanel(VMMainPanel vmMainPanel) {
		this.vmMainPanel = vmMainPanel;

		new Thread(this).start();
	}

	public void init(String searchPattern, int maxVMColumnCount, boolean refreshServerList) {
		if (maxVMColumnCount == 0 || vmIcons == null) {
			return;
		}
		this.searchPattern = searchPattern;
		this.maxVMColumnCount = maxVMColumnCount;
		if (searchPattern != null) {
			searchPattern = searchPattern.trim();
		}
		TitanSetting.getInstance().maxVMColumnCount = maxVMColumnCount;
		TitanSetting.getInstance().save();

		//		updateInstanceTypeComboBox();

		vmMainPanel.colLabel.setText(TitanSetting.getInstance().maxVMColumnCount + " columns");
		if (refreshServerList) {
			boolean hasNew = reAddVMIconsToVector();
			if (!hasNew) {
				return;
			}
		}

		if (servers == null) {
			return;
		}

		int noOfVM = servers.size();
		int maxCol = maxVMColumnCount;
		int maxRow = (int) Math.ceil(((float) noOfVM) / maxVMColumnCount);
		String colStr = StringUtils.repeat("[]", maxCol);
		String rowStr = StringUtils.repeat("[]", maxRow);
		//		System.out.println(noOfVM + "," + maxRow + "," + maxCol);
		vmMainPanel.iconPanel.setLayout(new MigLayout("", colStr, rowStr));

		removeAll();
		int row = 0;
		int col = 0;
		synchronized (vmIcons) {
			for (VMIcon vmIcon : vmIcons) {
				String name = TitanCommonLib.getJSONString(vmIcon.json, "name", null);
				if (searchPattern == null || searchPattern.equals("") || name.toLowerCase().contains(searchPattern.toLowerCase())) {
					add(vmIcon, "cell " + col + " " + row + ",aligny top");
					col++;
					if (col == maxCol) {
						col = 0;
						row++;
					}
				}
			}
		}
		updateUI();
	}

	private void updateInstanceTypeComboBox() {
		Command command = new Command();
		command.command = "get all instance types";
		ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
		if (r == null || r.map == null || r.map.get("result") == null) {
			return;
		}
		instanceTypes = (List<InstanceType>) r.map.get("result");

		if (vmMainPanel.groupComboBox != null && instanceTypes != null) {
			ArrayList<String> groupNames = new ArrayList<String>();
			for (InstanceType instanceType : instanceTypes) {
				if (instanceType != null && instanceType.getGroupName() != null) {
					groupNames.add(instanceType.getGroupName());
				}
			}
			// check new
			boolean hasNew = false;
			for (String s1 : groupNames) {
				boolean match = false;
				for (int x = 0; x < vmMainPanel.groupComboBox.getItemCount(); x++) {
					String s2 = (String) vmMainPanel.groupComboBox.getItemAt(x);
					if (s1 != null && s2 != null && s1.equals(s2)) {
						match = true;
						break;
					}
				}
				if (!match) {
					hasNew = true;
				}
			}
			// end check new
			if (hasNew) {
				Collections.sort(groupNames);
				vmMainPanel.groupComboBox.setModel(new DefaultComboBoxModel(groupNames.toArray()));
			}
		}
	}

	private boolean reAddVMIconsToVector() {
		//		synchronized (vmIcons) {
		Command command = new Command();
		command.command = "from titan: nova list";
		ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
		if (r == null || r.map == null) {
			return false;
		}
		HttpResult httpResult = (HttpResult) r.map.get("result");
		servers = JSONObject.fromObject(httpResult.content).getJSONArray("servers");
		// check new vm created or deleted
		boolean hasNew = false;
		if (servers.size() != vmIcons.size()) {
			hasNew = true;
		} else {
			for (int x = 0; x < servers.size(); x++) {
				JSONObject obj = servers.getJSONObject(x);
				//					String tempInstanceId = TitanCommonLib.getJSONString(obj, "id", null);
				boolean match = false;
				outer1: for (VMIcon vmIcon : vmIcons) {
					String instanceId = TitanCommonLib.getJSONString(vmIcon.json, "id", null);
					vmIcon.setVmType(findInstanceType(instanceId));

					if (obj.toString().equals(vmIcon.json.toString())) {
						match = true;
						break outer1;
					}
				}
				if (!match) {
					hasNew = true;
				}
			}
		}
		if (!hasNew) {
			return false;
		}
		// end check new vm created or deleted			

		vmIcons = new Vector<VMIcon>();
		for (Component c : getComponents()) {
			vmIcons.add((VMIcon) c);
		}

		Vector<VMIcon> willRemove = new Vector<VMIcon>();
		outer: for (VMIcon vmIcon : vmIcons) {
			String instanceId = TitanCommonLib.getJSONString(vmIcon.json, "id", null);
			for (int x = 0; x < servers.size(); x++) {
				JSONObject obj = servers.getJSONObject(x);
				String tempInstanceId = TitanCommonLib.getJSONString(obj, "id", null);
				if (instanceId.equals(tempInstanceId)) {
					vmIcon.json = obj;
					vmIcon.updateVMName();
					vmIcon.setVmType(findInstanceType(tempInstanceId));
					continue outer;
				}
			}
			willRemove.add(vmIcon);
		}
		vmIcons.removeAll(willRemove);

		outer: for (int x = 0; x < servers.size(); x++) {
			JSONObject obj = servers.getJSONObject(x);
			String instanceId = TitanCommonLib.getJSONString(obj, "id", null);
			for (VMIcon vmIcon : vmIcons) {
				String tempInstanceId = TitanCommonLib.getJSONString(vmIcon.json, "id", null);
				if (instanceId.equals(tempInstanceId)) {
					continue outer;
				}
			}
			VMIcon vmIcon = new VMIcon(obj);
			String tempInstanceId = TitanCommonLib.getJSONString(obj, "id", null);
			vmIcon.setVmType(findInstanceType(tempInstanceId));
			vmIcon.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					clearAllPanelsSelection();
					VMIcon panel = (VMIcon) e.getSource();
					vmMainPanel.selectedVM = panel.json;
					if ((e.getModifiers() & InputEvent.CTRL_MASK) == InputEvent.CTRL_MASK || (e.getModifiers() & InputEvent.META_MASK) == InputEvent.META_MASK) {
						panel.setClicked(!panel.clicked);
					}
					panel.setSelected(true);
					if (vmMainPanel.isUpdatePropertyTableThreadRunning) {
						vmMainPanel.isUpdatePropertyTableThreadTrigger = true;
					}
					new Thread(vmMainPanel).start();
				}

				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 2 && !SwingUtilities.isRightMouseButton(e)) {
						vmMainPanel.remote();
					}
				}
			});
			vmIcons.add(vmIcon);
		}

		Collections.sort(vmIcons);
		//		}
		return true;
	}

	private void clearAllPanelsSelection() {
		synchronized (vmIcons) {
			for (VMIcon panel : vmIcons) {
				panel.setSelected(false);
			}
		}
	}

	public void run() {
		while (true) {
			init(searchPattern, maxVMColumnCount, true);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Vector<JSONObject> getSelectedVM() {
		Vector<JSONObject> r = new Vector<JSONObject>();
		synchronized (vmIcons) {
			for (VMIcon vmPanel : vmIcons) {
				if (vmPanel.clicked) {
					r.add(vmPanel.json);
				}
			}
			if (r.size() == 0) {
				for (VMIcon vmPanel : vmIcons) {
					if (vmPanel.selected) {
						r.add(vmPanel.json);
					}
				}
			}
		}
		return r;
	}

	public String findInstanceType(String instanceId) {
		if (instanceTypes == null) {
			return null;
		} else {
			for (InstanceType instanceType : instanceTypes) {
				if (instanceType.getInstanceID().equals(instanceId)) {
					return instanceType.getType();
				}
			}
		}
		return null;
	}

	public void selectAll() {
		for (VMIcon vmPanel : vmIcons) {
			vmPanel.clicked = true;
			vmPanel.repaint();
		}
	}

	public void unselectAll() {
		for (VMIcon vmPanel : vmIcons) {
			vmPanel.clicked = false;
			vmPanel.repaint();
		}
	}
}
