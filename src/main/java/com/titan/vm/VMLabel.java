package com.titan.vm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import com.titan.OS;
import com.titan.TitanCommonLib;

public class VMLabel extends JLabel {
	VMIcon vmPanel;
	OS os;
	//	Icon icon = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/vmIcon.png"));
	Image normal = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/normal.png")).getImage();
	Image errorBG = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/errorBG.png")).getImage();

	Image gray = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/gray.png")).getImage();
	Image blue = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/blue.png")).getImage();
	Image yellow = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/yellow.png")).getImage();
	Image red = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/red.png")).getImage();

	Image noname = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/os/noname.png")).getImage();
	Image redhat = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/os/redhat.png")).getImage();
	Image ubuntu = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/os/ubuntu.png")).getImage();
	Image suse = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/os/suse.png")).getImage();
	Image centos = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/os/centos.png")).getImage();
	Image windows = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/os/windows.png")).getImage();
	Image linux = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/os/linux.png")).getImage();
	Image unix = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/os/unix.png")).getImage();

	Image building = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/status/building.png")).getImage();
	Image error = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/status/error.png")).getImage();
	Image unknown = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/status/error.png")).getImage();
	Image play = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/status/play.png")).getImage();
	Image pause = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/status/pause.png")).getImage();
	Image stop = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/status/stop.png")).getImage();
	Image reboot = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/status/reboot.png")).getImage();

	Image low = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/low.png")).getImage();
	Image middle = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/middle.png")).getImage();
	Image high = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/high.png")).getImage();

	Image shutoff = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/shutoff.png")).getImage();
	Image tick = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/tick.png")).getImage();
	Color normalTextColor = new Color(0, 162, 222);
	Color grayTextColor = new Color(193, 193, 193);

	public VMLabel(VMIcon vmPanel) {
		this(vmPanel, OS.none);
	}

	public VMLabel(VMIcon vmPanel, OS os) {
		super();
		this.vmPanel = vmPanel;
		setOS(os);
		setPreferredSize(new Dimension(normal.getWidth(null), normal.getHeight(null)));
		setOpaque(false);
	}

	public void setOS(OS os) {
		this.os = os;
	}

	public void paint(Graphics g) {
		super.paint(g);

		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		//		int cpu = new Random().nextInt(20);
		//		int memory = new Random().nextInt(20);

		String status = TitanCommonLib.getJSONString(vmPanel.json, "status", "SHUTOFF");
		String taskState = TitanCommonLib.getJSONString(vmPanel.json, "OS-EXT-STS:task_state", "");
		if (status.equals("ERROR") || status.equals("REBOOT") || taskState.equals("rebooting")) {
			g.drawImage(errorBG, 0, 0, null);
		} else {
			g.drawImage(normal, 0, 0, null);
		}

		if (vmPanel.vmType != null) {
			try {
				os = OS.valueOf(vmPanel.vmType);
			} catch (Exception ex) {
				os = OS.none;
			}
		}

		int offsetY = 4;
		int col1OffsetX = 6;
		int col2OffsetX = 51;
		if (os == OS.redhat) {
			g.drawImage(redhat, (44 - redhat.getWidth(null)) / 2 + col1OffsetX, (44 - redhat.getHeight(null)) / 2 + offsetY, null);
		} else if (os == OS.ubuntu) {
			g.drawImage(ubuntu, (44 - ubuntu.getWidth(null)) / 2 + col1OffsetX, (44 - ubuntu.getHeight(null)) / 2 + offsetY, null);
		} else if (os == OS.suse) {
			g.drawImage(suse, (44 - suse.getWidth(null)) / 2 + col1OffsetX, (44 - suse.getHeight(null)) / 2 + offsetY, null);
		} else if (os == OS.centos) {
			g.drawImage(centos, (44 - centos.getWidth(null)) / 2 + col1OffsetX, (44 - centos.getHeight(null)) / 2 + offsetY, null);
		} else if (os == OS.windows) {
			g.drawImage(windows, (44 - windows.getWidth(null)) / 2 + col1OffsetX, (44 - windows.getHeight(null)) / 2 + offsetY, null);
		} else if (os == OS.linux) {
			g.drawImage(linux, (44 - linux.getWidth(null)) / 2 + col1OffsetX, (44 - linux.getHeight(null)) / 2 + offsetY, null);
		} else if (os == OS.unix) {
			g.drawImage(unix, (44 - unix.getWidth(null)) / 2 + col1OffsetX, (44 - unix.getHeight(null)) / 2 + offsetY, null);
		} else {
			g.drawImage(noname, (44 - noname.getWidth(null)) / 2 + col1OffsetX, (44 - noname.getHeight(null)) / 2 + offsetY, null);
		}
		//		System.out.println(status + "=" + taskState);
		if (status.equals("REBOOT") || taskState.equals("rebooting")) {
			Font font = new Font("Arial", Font.PLAIN, 20);
			FontMetrics metrics = g.getFontMetrics(font);
			g.setFont(font);
			g.drawImage(reboot, 57, 7, null);
			g.setColor(normalTextColor);
			String str = "reboot";
			int strWidth = metrics.stringWidth(str);
			g.drawString(str, (88 - strWidth) / 2 + col1OffsetX, 77);
		} else if (status.equals("SHUTOFF")) {
			g.drawImage(stop, 57, 7, null);
		} else if (status.equals("BUILD")) {
			g.drawImage(building, 57, 7, null);
		} else if (status.equals("ERROR")) {
			Font font = new Font("Arial", Font.PLAIN, 24);
			FontMetrics metrics = g.getFontMetrics(font);
			g.setFont(font);
			g.drawImage(error, 57, 13, null);
			g.setColor(Color.red);
			int cpuFontWidth = metrics.stringWidth("ERROR");
			g.drawString("Error", (116 - cpuFontWidth) / 2 + col1OffsetX, 78);
		} else if (status.equals("ACTIVE")) {
			Font font = new Font("Arial", Font.PLAIN, 18);
			FontMetrics metrics = g.getFontMetrics(font);
			g.setFont(font);
			g.drawImage(play, 57, 10, null);
			g.setColor(normalTextColor);
			String cpuStr = String.valueOf((int) vmPanel.cpu) + "%";
			String memStr = String.valueOf(TitanCommonLib.round((int) vmPanel.mem / 1024, 1));
			if (memStr.endsWith(".0")) {
				memStr = memStr.replaceAll(".0", "");
			}
			memStr += "g";
			int cpuFontWidth = metrics.stringWidth(cpuStr);
			int memoryFontWidth = metrics.stringWidth(memStr);
			g.drawString(cpuStr, (44 - cpuFontWidth) / 2 + col1OffsetX, 77);
			g.drawString(memStr, (44 - memoryFontWidth) / 2 + col2OffsetX, 77);
		} else {
			g.drawImage(unknown, 57, 13, null);
		}

		if (vmPanel.clicked) {
			g.drawImage(tick, 70, 0, null);
		}
	}
}
