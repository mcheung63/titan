package com.titan.vm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;

import net.sf.json.JSONObject;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import com.peterswing.advancedswing.enhancedtextarea.EnhancedTextArea;
import com.peterswing.advancedswing.searchtextfield.JSearchTextField;
import com.titan.TitanCommonLib;
import com.titan.communication.CommunicateLib;
import com.titancommon.Command;
import com.titancommon.HttpResult;
import com.titancommon.ReturnCommand;
import com.titanserver.table.InstanceDiagnostics;
import com.titanserver.table.RestServerVmStat;
import com.toedter.calendar.JDateChooser;

public class VMLogDialog extends JDialog {
	private final JPanel contentPanel = new JPanel();
	JFrame frame;
	String instanceId;
	String instanceName;
	private EnhancedTextArea textArea;
	private ChartPanel chartPanel;
	TimeSeriesCollection dataset = new TimeSeriesCollection();
	JFreeChart chart = ChartFactory.createTimeSeriesChart("", "", "%", dataset, true, false, false);
	private JDateChooser fromDateChooser;
	private JComboBox<String> fromComboBox;
	private JDateChooser toDateChooser;
	private JComboBox periodComboBox;
	private JComboBox<String> toComboBox;
	JScrollPane scrollPane = new JScrollPane();
	private JTable detailTable;
	VMLogDetailTableModel vmLogDetailTableModel = new VMLogDetailTableModel();
	JPanel topPanel = new JPanel();
	JPanel bottomPanel = new JPanel();
	private JSearchTextField searchTextField;
	/**
	 * @wbp.nonvisual location=683,31
	 */
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton cpuRadioButton;
	private JRadioButton memoryRadioButton;

	public VMLogDialog(JFrame frame, String instanceId, String instanceName) {
		super(frame, false);
		this.frame = frame;
		this.instanceId = instanceId;
		this.instanceName = instanceName;
		setTitle(instanceName);

		setBounds(100, 100, 900, 750);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			contentPanel.add(tabbedPane, BorderLayout.CENTER);
			JPanel diagnosticPanel = new JPanel();
			tabbedPane.addTab("Diagnostic", null, diagnosticPanel, null);
			diagnosticPanel.setLayout(new BorderLayout(0, 0));
			{
				bottomPanel.setLayout(new BorderLayout(0, 0));
				{
					detailTable = new JTable();
					detailTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
					detailTable.setModel(vmLogDetailTableModel);
					vmLogDetailTableModel.instanceId = instanceId;
					new Thread(vmLogDetailTableModel).start();
					detailTable.getColumnModel().getColumn(0).setPreferredWidth(80);
					detailTable.getColumnModel().getColumn(1).setPreferredWidth(150);
					detailTable.getColumnModel().getColumn(2).setPreferredWidth(150);
					detailTable.getColumnModel().getColumn(3).setPreferredWidth(150);
					detailTable.getColumnModel().getColumn(4).setPreferredWidth(150);
					detailTable.getColumnModel().getColumn(5).setPreferredWidth(800);
					DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
					centerRenderer.setHorizontalAlignment(JTextField.CENTER);
					DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
					rightRenderer.setHorizontalAlignment(JTextField.RIGHT);
					detailTable.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
					detailTable.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
					detailTable.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
					detailTable.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
					{
						JPanel searchPanel = new JPanel();
						FlowLayout flowLayout = (FlowLayout) searchPanel.getLayout();
						flowLayout.setAlignment(FlowLayout.LEFT);
						bottomPanel.add(searchPanel, BorderLayout.NORTH);
						{
							JLabel lblSearch = new JLabel("Search");
							searchPanel.add(lblSearch);
						}
						{
							searchTextField = new JSearchTextField();
							searchTextField.setPreferredSize(new Dimension(150, 25));
							searchPanel.add(searchTextField);
						}
					}

					bottomPanel.add(scrollPane, BorderLayout.CENTER);
					scrollPane.setViewportView(detailTable);
				}
			}
			{
				JSplitPane splitPane = new JSplitPane();
				splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
				splitPane.add(topPanel, JSplitPane.TOP);
				splitPane.add(bottomPanel, JSplitPane.BOTTOM);
				splitPane.setDividerLocation(400);
				diagnosticPanel.add(splitPane, BorderLayout.CENTER);
			}
			{
				topPanel.setLayout(new BorderLayout(0, 0));
				JPanel toolbarPanel = new JPanel();
				topPanel.add(toolbarPanel, BorderLayout.NORTH);
				FlowLayout fl_toolbarPanel = (FlowLayout) toolbarPanel.getLayout();
				fl_toolbarPanel.setAlignment(FlowLayout.LEFT);
				{
					JLabel label = new JLabel("From");
					toolbarPanel.add(label);
				}
				{
					fromDateChooser = new JDateChooser();
					fromDateChooser.setDateFormatString("yyyy/MM/dd");
					fromDateChooser.setDate(new Date());
					fromDateChooser.setPreferredSize(new Dimension(150, 25));
					toolbarPanel.add(fromDateChooser);
				}
				fromComboBox = new JComboBox<String>();
				toolbarPanel.add(fromComboBox);
				{
					JLabel label = new JLabel("To");
					toolbarPanel.add(label);
				}
				{
					toDateChooser = new JDateChooser();
					toDateChooser.setDateFormatString("yyyy/MM/dd");
					toDateChooser.setDate(new Date());
					toDateChooser.setPreferredSize(new Dimension(150, 25));
					toolbarPanel.add(toDateChooser);
				}
				toComboBox = new JComboBox<String>();
				toolbarPanel.add(toComboBox);
				{
					periodComboBox = new JComboBox(new String[] { "mintue", "hour", "day", "month" });
					toolbarPanel.add(periodComboBox);
				}
				{
					JButton button = new JButton("Refresh");
					button.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							initChart();
						}
					});
					{
						cpuRadioButton = new JRadioButton("cpu");
						buttonGroup.add(cpuRadioButton);
						cpuRadioButton.setSelected(true);
						toolbarPanel.add(cpuRadioButton);
					}
					{
						memoryRadioButton = new JRadioButton("memory");
						buttonGroup.add(memoryRadioButton);
						toolbarPanel.add(memoryRadioButton);
					}
					toolbarPanel.add(button);
				}
				{
					JLabel label = new JLabel("");
					toolbarPanel.add(label);
				}
				chartPanel = new ChartPanel(chart);
				topPanel.add(chartPanel, BorderLayout.CENTER);
			}
			{
				JPanel panel = new JPanel();
				tabbedPane.addTab("Log", null, panel, null);
				panel.setLayout(new BorderLayout(0, 0));
				{
					textArea = new EnhancedTextArea();
					panel.add(textArea, BorderLayout.CENTER);
				}
			}
			{
				for (int x = 0; x < 24; x++) {
					fromComboBox.addItem(String.format("%02d", x) + ":00");
				}
				for (int x = 0; x < 24; x++) {
					toComboBox.addItem(String.format("%02d", x) + ":59");
				}
				toComboBox.setSelectedItem("23:59");
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton closeButton = new JButton("Close");
				closeButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				closeButton.setActionCommand("OK");
				buttonPane.add(closeButton);
				getRootPane().setDefaultButton(closeButton);
			}
		}
		setLocationRelativeTo(null);

		init();
	}

	private void init() {
		try {
			Command command = new Command();
			command.command = "from titan: nova show";
			HashMap<String, String> parameters = new HashMap<String, String>();
			parameters.put("$InstanceId", instanceId);
			command.parameters.add(parameters);
			ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
			if (r != null && r.map.get("result") != null) {
				JSONObject json = JSONObject.fromObject(((HttpResult) r.map.get("result")).content).getJSONObject("server").getJSONObject("fault");
				textArea.setText(json.getString("message") + "\n\n" + json.getString("details"));
			}
		} catch (Exception ex) {
		}

		initChart();
	}

	private void initChart() {
		try {
			Command command = new Command();
			command.command = "getInstanceDiagnostics";
			command.parameters.add(instanceId);
			Date fromDate = fromDateChooser.getDate();
			fromDate.setHours(Integer.parseInt(fromComboBox.getSelectedItem().toString().split(":")[0]));
			fromDate.setMinutes(Integer.parseInt(fromComboBox.getSelectedItem().toString().split(":")[1]));
			fromDate.setSeconds(0);
			Date toDate = toDateChooser.getDate();
			toDate.setHours(Integer.parseInt(toComboBox.getSelectedItem().toString().split(":")[0]));
			toDate.setMinutes(Integer.parseInt(toComboBox.getSelectedItem().toString().split(":")[1]));
			toDate.setSeconds(59);
			command.parameters.add(fromDate);
			command.parameters.add(toDate);
			command.parameters.add(periodComboBox.getSelectedItem());
			ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
			if (r == null || r.map == null) {
				return;
			}
			List<InstanceDiagnostics> list = (List<InstanceDiagnostics>) r.map.get("result");
			if (list != null) {
				if (cpuRadioButton.isSelected()) {
					TimeSeries vmSeries = new TimeSeries("cpu");

					for (int x = 0; x < list.size(); x++) {
						try {
							InstanceDiagnostics s = list.get(x);
							Minute min = new Minute(s.getDate());

							vmSeries.add(min, TitanCommonLib.round(s.getPercent(), 1));
						} catch (Exception ex) {
						}
					}
					dataset.removeAllSeries();
					dataset.addSeries(vmSeries);
				} else if (memoryRadioButton.isSelected()) {
					TimeSeries residentSeries = new TimeSeries("memory");
					//					TimeSeries shareSeries = new TimeSeries("share");
					//					TimeSeries usedSeries = new TimeSeries("used");
					for (int x = 0; x < list.size(); x++) {
						try {
							InstanceDiagnostics s = list.get(x);
							Minute min = new Minute(s.getDate());

							residentSeries.add(min, TitanCommonLib.round(((double) s.getResident()) / s.getSysTotal() * 100, 1));
							//							shareSeries.add(min, TitanCommonLib.round(((double) s.getShare()) / s.getSysTotal() * 100, 1));
							//							usedSeries.add(min, TitanCommonLib.round(((double) s.getSysUsed()) / s.getSysTotal() * 100, 1));
						} catch (Exception ex) {
						}
					}
					dataset.removeAllSeries();
					dataset.addSeries(residentSeries);
					//					dataset.addSeries(shareSeries);
					//					dataset.addSeries(usedSeries);
				}

				// all processes timeseries
				if (detailTable.getSelectedRowCount() > 0) {
					Command command2 = new Command();
					command2.command = "getVMStats";
					command2.parameters.add(instanceId);
					command2.parameters.add(fromDate);
					command2.parameters.add(toDate);
					//					command2.parameters.add(periodComboBox.getSelectedItem());
					ReturnCommand r2 = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command2);
					List<RestServerVmStat> processList = (List<RestServerVmStat>) r2.map.get("result");
					System.out.println("finished getVMStats");
					if (processList != null && processList.size() > 0) {
						for (int x = 0; x < detailTable.getSelectedRowCount(); x++) {
							String pid = (String) detailTable.getValueAt(detailTable.getSelectedRows()[x], vmLogDetailTableModel.getColumnIndex("pid"));
							TimeSeries processSeries = new TimeSeries(pid);
							dataset.addSeries(processSeries);
							for (RestServerVmStat restServerVmStat : processList) {
								JSONObject json = JSONObject.fromObject(restServerVmStat.getMessage()).getJSONObject("result").getJSONObject("processes").getJSONObject(pid);

								Minute min = new Minute(restServerVmStat.getDate());
								if (cpuRadioButton.isSelected()) {
									processSeries.addOrUpdate(min, TitanCommonLib.round(Double.parseDouble(getValue(json.getJSONObject("procCpu"), "percent")), 1));
								} else if (memoryRadioButton.isSelected()) {
									processSeries.addOrUpdate(min, TitanCommonLib.round(Double.parseDouble(getValue(json.getJSONObject("procMem"), "resident")), 1));
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		changeChartStyle(chart);
	}

	private void changeChartStyle(JFreeChart chart) {
		XYPlot plot = chart.getXYPlot();
		plot.setBackgroundPaint(Color.white);
		plot.getRenderer().setSeriesPaint(0, new Color(0, 126, 255));
		plot.setDomainGridlinePaint(Color.lightGray);
		plot.setRangeGridlinePaint(Color.lightGray);
	}

	private String getValue(JSONObject value, String key) {
		try {
			return value.getString(key);
		} catch (Exception ex) {
			return null;
		}
	}
}
