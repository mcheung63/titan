package com.titan.vm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import net.sf.json.JSONObject;

import com.titan.TitanCommonLib;
import com.titan.communication.CommunicateLib;
import com.titancommon.Command;
import com.titancommon.ReturnCommand;
import com.titanserver.table.InstanceDiagnostics;
import com.titanserver.table.InstanceType;

public class VMIcon extends JPanel implements Comparable, Runnable {
	JLabel label = new JLabel("");
	public boolean clicked;
	public boolean selected;
	LineBorder border = new LineBorder(new Color(0, 0, 0), 1);
	EmptyBorder emptyBorder = new EmptyBorder(1, 1, 1, 1);
	JSONObject json;
	Image selectedImage = new ImageIcon(getClass().getClassLoader().getResource("com/titan/image/vmPanel/selected.png")).getImage();
	String vmName;
	String vmType;
	public int maxVMNameLength = 15;
	double cpu;
	double mem;

	public VMIcon(JSONObject json) {
		this.json = json;
		VMLabel iconLabel = new VMLabel(this);
		setLayout(new BorderLayout(0, 0));

		add(iconLabel, BorderLayout.CENTER);

		updateVMName();

		label.setHorizontalAlignment(SwingConstants.CENTER);
		add(label, BorderLayout.SOUTH);
		setBorder(emptyBorder);
		setMaximumSize(new Dimension(100, 200));
		setOpaque(false);
		new Thread(this).start();
	}

	public void updateVMName() {
		vmName = TitanCommonLib.getJSONString(json, "name", "No name");
		label.setText("<html><body><center>" + vmName.replaceAll("(.{" + maxVMNameLength + "})", "$1<br>") + "</center></body></html>");
	}

	public void setVmType(String vmType) {
		this.vmType = vmType;
	}

	public void paint(Graphics g) {
		if (selected) {
			g.drawImage(selectedImage, 0, 0, null);
		}

		super.paint(g);
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
		repaint();
	}

	public void setClicked(boolean clicked) {
		this.clicked = clicked;
		repaint();
	}

	@Override
	public int compareTo(Object o) {
		return vmName.compareToIgnoreCase(((VMIcon) o).vmName);
	}

	@Override
	public void run() {
		String instanceId = TitanCommonLib.getJSONString(json, "id", null);
		while (true) {
			Command command = new Command();
			command.command = "get instance type";
			command.parameters.add(instanceId);
			ReturnCommand r = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command);
			InstanceType instanceType = (InstanceType) r.map.get("result");
			if (instanceType != null) {
				vmType = instanceType.getType();
			}
			repaint();
			
			
			Command command2 = new Command();
			command2.command = "get instance stat";
			command2.parameters.add(instanceId);
			ReturnCommand r2 = CommunicateLib.send(TitanCommonLib.getCurrentServerIP(), command2);
			if (r2 != null && r2.map.get("result") != null) {
				InstanceDiagnostics instanceDiagnostics = (InstanceDiagnostics) r2.map.get("result");
				cpu = instanceDiagnostics.getPercent();
				//mem = ((double) instanceDiagnostics.getResident()) / instanceDiagnostics.getSysTotal() * 100;
				mem = ((double) instanceDiagnostics.getResident()) / 1024 / 1024;
			}
			repaint();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
