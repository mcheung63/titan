titan
=====

Cloud oriented programming based on Openstack.

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2014/03/vm-log-dialog-for-windows.png)

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2014/02/Titan-Screenshot-from-2014-02-25-020714.png)

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2014/02/Screen-Shot-2014-02-04-at-6.24.11-pm.png)

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2014/02/Screen-Shot-2014-02-04-at-6.24.15-pm.png)
